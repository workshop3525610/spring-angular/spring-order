package com.testingproject.order.service;

import com.testingproject.order.dao.OrderDao;
import com.testingproject.order.dto.OrderDto;
import com.testingproject.order.entity.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderDao dao;

    public Order save(OrderDto.Save data){
        Order order = this.dao.save(data);
        return order;
    }

    public Order update(Integer orderId, OrderDto.Save data){
        Order order = this.dao.update(orderId, data);
        return order;
    }

    public List<Order> findAll(){
        List<Order> orders = this.dao.findAll();
        return orders;
    }

    public List<Order> findByCustomerId(Integer customerId){
        List<Order> orders = this.dao.findByCustomerId(customerId);
        return orders;
    }

    public Order findById(Integer orderId) {
        Order order = this.dao.findById(orderId);
        return order;
    }

    public void deleteById(Integer orderId) {
        this.dao.deleteByOrderId(orderId);
    }

}
