package com.testingproject.order.dao;

import com.testingproject.order.dto.OrderDto;
import com.testingproject.order.entity.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class OrderDao {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Order save(OrderDto.Save data){
        String query = "INSERT INTO public.orders\n" +
                "(customer_id, product, quantity)\n" +
                "VALUES(:customerId, :product, :quantity)";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("customerId", data.getCustomerId());
        map.addValue("product", data.getProduct());
        map.addValue("quantity", data.getQuantity());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        this.namedParameterJdbcTemplate.update(query, map, keyHolder);


        Order value = new Order();
        value.setId((int) keyHolder.getKeys().get("id"));
        value.setCustomerId(data.getCustomerId());
        value.setProduct(data.getProduct());
        value.setQuantity(data.getQuantity());

        return value;
    }

    public Order update(Integer orderId, OrderDto.Save data){
        String query = "UPDATE public.orders\n" +
                "SET product=:product, quantity=:quantity\n" +
                "WHERE id=:orderId";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("orderId", orderId);
        map.addValue("product", data.getProduct());
        map.addValue("quantity", data.getQuantity());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        this.namedParameterJdbcTemplate.update(query, map, keyHolder);


        Order value = new Order();
        value.setId((int) keyHolder.getKeys().get("id"));
        value.setCustomerId(data.getCustomerId());
        value.setProduct(data.getProduct());
        value.setQuantity(data.getQuantity());

        return value;
    }

    public List<Order> findAll(){
        String query = "SELECT id, customer_id, product, quantity\n" +
                "FROM public.orders";

        return this.namedParameterJdbcTemplate.query(query, new RowMapper<Order>() {
            @Override
            public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
                Order value = new Order();
                value.setId(rs.getInt("id"));
                value.setCustomerId(rs.getInt("customer_id"));
                value.setProduct(rs.getString("product"));
                value.setQuantity(rs.getInt("quantity"));
                return value;
            }
        });
    }

    public Order findById(Integer orderId) {
        String query = "SELECT id, customer_id, product, quantity\n" +
                "FROM public.orders where id = :orderId";

        SqlParameterSource param = new MapSqlParameterSource("orderId", orderId);

        Order result = this.namedParameterJdbcTemplate.query(query, param, new ResultSetExtractor<Order>() {
            @Override
            public Order extractData(ResultSet rs) throws SQLException, DataAccessException {
                if (rs.next()) {
                    Order order = new Order();
                    order.setId(rs.getInt("id"));
                    order.setCustomerId(rs.getInt("customer_id"));
                    order.setProduct(rs.getString("product"));
                    order.setQuantity(rs.getInt("quantity"));
                    return order;
                }
                return null;
            }
        });

        return result;
    }


    public List<Order> findByCustomerId(Integer customerId){
        String query = "SELECT id, customer_id, product, quantity\n" +
                "FROM public.orders where customer_id=:customerId";
        MapSqlParameterSource map = new MapSqlParameterSource("customerId", customerId);

        return this.namedParameterJdbcTemplate.query(query, map, new RowMapper<Order>() {
            @Override
            public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
                Order value = new Order();
                value.setId(rs.getInt("id"));
                value.setCustomerId(rs.getInt("customer_id"));
                value.setProduct(rs.getString("product"));
                value.setQuantity(rs.getInt("quantity"));
                return value;
            }
        });
    }

    public void deleteByOrderId(Integer orderId){
        String query = "delete FROM public.orders where id=:orderId";
        MapSqlParameterSource map = new MapSqlParameterSource("orderId", orderId);

        this.namedParameterJdbcTemplate.update(query, map);
    }

}
