package com.testingproject.order.controller;

import com.testingproject.order.dto.OrderDto;
import com.testingproject.order.entity.Order;
import com.testingproject.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/order")
public class OrderController {

    private final OrderService service;

    @PostMapping("/save")
    public ResponseEntity<Order> save(
            @RequestBody OrderDto.Save data
    ) {
        Order order = this.service.save(data);
        return ResponseEntity.ok(order);
    }

    @PutMapping("/update/{orderId}")
    public ResponseEntity<Order> save(
            @RequestBody OrderDto.Save data, @PathVariable("orderId") Integer orderId
    ) {
        Order order = this.service.update(orderId, data);
        return ResponseEntity.ok(order);
    }

    @GetMapping("/list")
    public ResponseEntity<List<Order>> findAll(){
        List<Order> orders = this.service.findAll();
        return ResponseEntity.ok(orders);
    }

    @GetMapping("/list/{customerId}")
    public ResponseEntity<List<Order>> findByCustomerId(
            @PathVariable("customerId") Integer customerId
    ){
        List<Order> orders = this.service.findByCustomerId(customerId);
        return ResponseEntity.ok(orders);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Order> findByOrderId(
            @PathVariable("orderId") Integer orderId
    ){
//        System.out.println("masukkkkkkkkkkkkkkkkkkkk findByOrderId");
//        return new ResponseEntity<Order>(HttpStatus.INTERNAL_SERVER_ERROR);
        Order order = this.service.findById(orderId);
//        System.out.println(order);
        return ResponseEntity.ok(order);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Order> delete(
            @PathVariable("id") Integer id
    ){
//        System.out.println("masukkkkkkkkkkkkkkkkkkkk delete");

        try {
            this.service.deleteById(id);
            return new ResponseEntity<Order>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Order>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
